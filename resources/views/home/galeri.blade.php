
@extends('layouts.home')

@section('content')
<section id="about-us" class="cta">
    <div class="container">
      <div class="d-flex justify-content-center align-items-end" style="height: 150px;">
        <div class="text-center">
          <h3>Galeri Foto</h3>
          <p>Berbagi dokumentasi dan foto komunitas</p>
        </div>
      </div>
    </div>
  </section>
  <section id="artikel">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <img src="<?= asset('assets/backend/img/example-image.jpg') ?>" alt="" class="img-fluid">
            </div>
            <div class="card-footer">
              <div class="d-flex flex-column">
                <span>Kegiatan Buka Bersama</span>
                <span>Sabtu, 26 Maret 2022</span>
              </div>

            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <img src="<?= asset('image/buka-bersama.jpg') ?>" alt="" class="img-fluid">
            </div>
            <div class="card-footer">
              <div class="d-flex flex-column">
                <span>Kegiatan Buka Bersama</span>
                <span>Sabtu, 26 Maret 2022</span>
              </div>

            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <img src="<?= asset('image/buka-bersama.jpg') ?>" alt="" class="img-fluid">
            </div>
            <div class="card-footer">
              <div class="d-flex flex-column">
                <span>Kegiatan Buka Bersama</span>
                <span>Sabtu, 26 Maret 2022</span>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
