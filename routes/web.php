<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', [App\Http\Controllers\C_home::class, 'index'])->name('home');
Route::get('/about', [App\Http\Controllers\C_home::class, 'about'])->name('about');
Route::get('/artikel', [App\Http\Controllers\C_home::class, 'artikel'])->name('artikel');
Route::get('/event', [App\Http\Controllers\C_home::class, 'event'])->name('event');
Route::get('/galeri', [App\Http\Controllers\C_home::class, 'galeri'])->name('galeri');
Route::get('/klien', [App\Http\Controllers\C_home::class, 'klien'])->name('klien');
Route::get('/logout', [App\Http\Controllers\C_login::class, 'logOut'])->name('logout');

Route::middleware(['guest'])->group(function () {
    Route::get('/login', [App\Http\Controllers\C_login::class, 'index'])->name('login');
    Route::post('/auth', [App\Http\Controllers\C_login::class, 'authLogin'])->name('auth');
});

Route::middleware(['auth'])->prefix('admin')->group(function () {
    Route::get('/', [App\Http\Controllers\C_admin::class, 'index'])->name('admin');
    Route::get('/home', [App\Http\Controllers\C_admin::class, 'home'])->name('home');

});
