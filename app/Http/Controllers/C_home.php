<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class C_home extends Controller
{
    function index()
    {
        return view('home.landing-page');
    }

    function artikel()
    {
        return view('home.artikel');
    }

    function event()
    {
        return view('home.event');
    }
    function galeri()
    {
        return view('home.galeri');
    }
    function klien()
    {
        return view('home.klien');
    }

    function about()
    {
        return view('home.about');
    }
}
