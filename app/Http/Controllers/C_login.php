<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class C_login extends Controller
{
    function index(Request $request)
    {
        return view('login.view-login');
    }

    function authLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->intended('/admin');
        }

        return back()->withErrors(['email' => 'Email atau password salah.']);
    }

    function logOut()
    {
        Auth::logout();
        return redirect('/login');
    }
}
